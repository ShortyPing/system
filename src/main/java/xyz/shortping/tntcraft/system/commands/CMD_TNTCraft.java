/**
 * Dieser Quelltext ist geistiges Eigentum von Michael Steinmötzger (ShortPing).
 * Alle Rechte unterliegen der Lizenz unter dieser, dieser Quelltext geführt wird.
 * Jegliche Vervielfältigungsrechte unterliegen dieser Lizenz.
 * <p>
 * This source code is the intellectual property of Michael Steinmötzger (ShortPing).
 * All rights are subject to the license under which this source code is licensed.
 * Any reproduction rights are subject to this license.
 * <p>
 * Copyright © Michael Steinmötzger 2018-2021
 * <p>
 * Alle Rechte vorbehalten
 * All rights reserved
 */

package xyz.shortping.tntcraft.system.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import xyz.shortping.tntcraft.system.TNTCraftSystem;

public class CMD_TNTCraft implements CommandExecutor {

    private final TNTCraftSystem plugin;

    public CMD_TNTCraft(TNTCraftSystem plugin) {
        this.plugin = plugin;
        this.plugin.getCommand("tntcraft").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        sender.sendMessage("                §4§lTNT§6§lCraft§e§l.eu");
        sender.sendMessage(plugin.getPrefix() + " §7Discord: §c/discord");
        sender.sendMessage(plugin.getPrefix() + " §7TeamSpeak: §cTNTCraft.eu");
        sender.sendMessage(plugin.getPrefix() + " §7Website: §chttps://TNTCraft.eu");
        sender.sendMessage("                §4§lTNT§6§lCraft§e§l.eu");
        return false;
    }
}
