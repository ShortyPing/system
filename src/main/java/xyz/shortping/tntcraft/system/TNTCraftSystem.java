/**
 * Dieser Quelltext ist geistiges Eigentum von Michael Steinmötzger (ShortPing).
 * Alle Rechte unterliegen der Lizenz unter dieser, dieser Quelltext geführt wird.
 * Jegliche Vervielfältigungsrechte unterliegen dieser Lizenz.
 * <p>
 * This source code is the intellectual property of Michael Steinmötzger (ShortPing).
 * All rights are subject to the license under which this source code is licensed.
 * Any reproduction rights are subject to this license.
 * <p>
 * Copyright © Michael Steinmötzger 2018-2021
 * <p>
 * Alle Rechte vorbehalten
 * All rights reserved
 */

package xyz.shortping.tntcraft.system;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.shortping.tntcraft.system.commands.CMD_TNTCraft;

public class TNTCraftSystem extends JavaPlugin {

    @Getter
    public String prefix = "§4§lTNT§6§lCraft§e§l.eu §7» ";
    @Getter
    public String consolePrefix = "[TNTCraft.eu] ";
    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage(consolePrefix + "§aPlugin has started");
        init();
    }

    @Override
    public void onDisable() {
        Bukkit.getConsoleSender().sendMessage(consolePrefix + "§aPlugin has stopped");
    }

    public void init() {
        new CMD_TNTCraft(this);
    }
}
